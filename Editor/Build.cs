using UnityEditor;
using System.Linq;

static class Build
{
	static void WebGL()
	{
		BuildPipeline.BuildPlayer(new BuildPlayerOptions()
		{
			scenes = (from scene in EditorBuildSettings.scenes where scene.enabled select scene.path).ToArray(),
			locationPathName = "web",
			target = BuildTarget.WebGL,
			options = BuildOptions.None
		});
	}
}
