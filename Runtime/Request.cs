using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;
using static Unity.Networking.Transport.NetworkEvent.Type;
using UnityEngine.InputSystem;
using System;

public class Request : ISystem
{
	public event Action<GameObject> OnClone;

	Dictionary<Client, NetworkDriver> drivers = new Dictionary<Client, NetworkDriver>();
	Dictionary<Client, NetworkConnection> connections = new Dictionary<Client, NetworkConnection>();

	List<role_client> clients = new List<role_client>();

	struct role_client
	{
		public GameObject _go;
		public Client client;
	}

	public void OnCreate (GameObject go)
	{
		if (go.GetComponent<Client>() is var client && client)
		{
			clients.Add(new role_client { _go = go, client = client });
		}
	}

	public void OnDestroy (GameObject go)
	{
		for (var i = clients.Count - 1; i >= 0; i--)
		{
			if (clients[i]._go == go)
			{
				clients.RemoveAt(i);
			}
		}
	}

	public void OnUpdate ()
	{
		for (var i = 0; i < clients.Count; i++)
		{
			var client = clients[i];

			if ( ! drivers.ContainsKey(client.client))
			{
				Debug.Log("Connecting");
				var driver = NetworkDriver.Create();
				var connection = driver.Connect(NetworkEndPoint.Parse(client.client.ip, client.client.port));
				drivers.Add(client.client, driver);
				connections.Add(client.client, connection);
			}
		}

		foreach (var client in drivers.Keys)
		{
			var driver = drivers[client];
			var connection = connections[client];

			driver.ScheduleUpdate().Complete();

			while (driver.PopEventForConnection(connection, out var reader) is var eventType && eventType != Empty)
			{
				if (eventType == Connect)
				{

				}
				else if (eventType == Data)
				{
					var command = reader.ReadByte();
					if (command == 0)
					{
						Serialization.Desync(ref reader);
					}
					else if (command == 1)
					{
						var prefab = Serialization.DeserializeGameObject(ref reader);
						var go = GameObject.Instantiate(prefab);
						Serialization.Include(go);

						OnClone?.Invoke(go);
					}
				}
			}
		}
	}

	public void Stop ()
	{
		foreach (var driver in drivers.Values)
		{
			driver.Dispose();
		}

		drivers.Clear();
		connections.Clear();
	}

	public void SendInput (InputAction input)
	{
		foreach (var client in drivers.Keys)
		{
			var driver = drivers[client];
			var connection = connections[client];

			driver.BeginSend(connection, out var writer);
			Serialization.Serialize(input, ref writer);
			writer.WriteFloat(input.ReadValue<float>());
			driver.EndSend(writer);
		}
	}
}
