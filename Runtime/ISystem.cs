using UnityEngine;

public interface ISystem
{
	void OnCreate (GameObject go);
	void OnDestroy (GameObject go);
	void OnUpdate ();
}
