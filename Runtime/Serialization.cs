using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;
using UnityEngine.InputSystem;

public class Serialization
{
	static uint gameobject_number = 0;
	static uint input_number = 0;

	static Dictionary<GameObject, uint> goToNumber = new Dictionary<GameObject, uint>();
	static Dictionary<uint, GameObject> numberToGo = new Dictionary<uint, GameObject>();

	static Dictionary<InputAction, uint> inputToNumber = new Dictionary<InputAction, uint>();
	static Dictionary<uint, InputAction> numberToInput = new Dictionary<uint, InputAction>();

	public static void Include (GameObject go)
	{
		if ( ! goToNumber.ContainsKey(go) && ! go.GetComponent<Server>() && ! go.GetComponent<Client>())
		{
			var number = gameobject_number++;
			goToNumber.Add(go, number);
			numberToGo.Add(number, go);
		}
	}

	public static void Include (InputAction input)
	{
		if ( ! inputToNumber.ContainsKey(input))
		{
			var number = input_number++;
			inputToNumber.Add(input, number);
			numberToInput.Add(number, input);
		}
	}

	public static void Serialize (GameObject go, ref DataStreamWriter writer)
	{
		writer.WriteUInt(goToNumber[go]);
	}

	public static GameObject DeserializeGameObject (ref DataStreamReader reader)
	{
		return numberToGo[reader.ReadUInt()];
	}

	public static void Serialize (InputAction input, ref DataStreamWriter writer)
	{
		writer.WriteUInt(inputToNumber[input]);
	}

	public static InputAction DeserializeInput (ref DataStreamReader reader)
	{
		return numberToInput[reader.ReadUInt()];
	}

	public static void Synchronize (ref DataStreamWriter writer)
	{
		for (uint number = 0; number < gameobject_number; number++)
		{
			var go = numberToGo[number];

			var position = go.transform.position;
			var rotation = go.transform.eulerAngles;

			writer.WriteUInt(number);

			writer.WriteFloat(position.x);
			writer.WriteFloat(position.y);
			writer.WriteFloat(position.z);

			writer.WriteFloat(rotation.x);
			writer.WriteFloat(rotation.y);
			writer.WriteFloat(rotation.z);
		}
	}

	public static void Desync (ref DataStreamReader reader)
	{
		for (uint number = 0; number < gameobject_number; number++)
		{
			var i = reader.ReadUInt();

			var go = numberToGo[i];

			var x = reader.ReadFloat();
			var y = reader.ReadFloat();
			var z = reader.ReadFloat();

			go.transform.position = new Vector3(x,y,z);

			x = reader.ReadFloat();
			y = reader.ReadFloat();
			z = reader.ReadFloat();

			go.transform.rotation = Quaternion.Euler(x,y,z);
		}
	}
}
