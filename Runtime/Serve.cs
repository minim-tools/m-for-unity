using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;
using static Unity.Networking.Transport.NetworkEvent.Type;
using System;
using UnityEngine.InputSystem;

public class Serve : ISystem
{
	Dictionary<uint, NetworkConnection> numberToConnection = new Dictionary<uint, NetworkConnection>();
	Dictionary<NetworkConnection,uint> connectionToNumber = new Dictionary<NetworkConnection,uint>();

	Dictionary<(uint,InputAction),float> inputs = new Dictionary<(uint,InputAction),float>();

	Dictionary<int, NetworkDriver> drivers = new Dictionary<int, NetworkDriver>();
	Dictionary<int, List<NetworkConnection>> connection_lists = new Dictionary<int, List<NetworkConnection>>();

	uint next_connection_id = 0;

	public struct role_port
	{
		public GameObject _go;
		public Server server;
	}

	public List<role_port> ports = new List<role_port>();

	public void OnCreate (GameObject go)
	{
		if (go.GetComponent<Server>() is var server && server)
		{
			ports.Add(new role_port { _go = go, server = server });
		}
	}

	public void OnDestroy (GameObject go)
	{
		for (var i = ports.Count - 1; i >= 0; i--)
		{
			if (ports[i]._go == go)
			{
				var driver = drivers[ports[i].server.port];
				driver.Dispose();
				drivers.Remove(ports[i].server.port);
				ports.RemoveAt(i);
			}
		}
	}

	public void OnUpdate ()
	{
		for (var i = 0; i < ports.Count; i++)
		{
			var port = ports[i];

			if ( ! drivers.ContainsKey(port.server.port))
			{
				var driver = NetworkDriver.Create();
				var error = driver.Bind(NetworkEndPoint.Parse("0.0.0.0", port.server.port));
				if (error != 0)
				{
					throw new Exception("Error listening on port "+port.server.port+". Try a different port");
				}
				driver.Listen();
				drivers.Add(port.server.port, driver);
				connection_lists.Add(port.server.port, new List<NetworkConnection>());
			}
		}

		foreach (var port in drivers.Keys)
		{
			var driver = drivers[port];
			var connections = connection_lists[port];

			driver.ScheduleUpdate().Complete();

			foreach (var connection in connections)
			{
				while (driver.PopEventForConnection(connection, out var reader) is var eventType && eventType != Empty)
				{
					if (eventType == Data)
					{
						var input = Serialization.DeserializeInput(ref reader);
						var value = reader.ReadFloat();
						var connection_number = connectionToNumber[connection];

						inputs[(connection_number, input)] = value;
					}
				}

				driver.BeginSend(connection, out var writer);
				writer.WriteByte(0);
				Serialization.Synchronize(ref writer);
				Debug.Log(writer.Length);
				driver.EndSend(writer);
			}
		}
	}

	public void Stop ()
	{
		foreach (var driver in drivers.Values)
		{
			driver.Dispose();
		}
		drivers.Clear();
	}

	public float AcceptConnection (float float_port)
	{
		var port = (int) float_port;

		if ( ! drivers.ContainsKey(port)) return Mathf.Infinity;

		var driver = drivers[(int) port];

		var connection = driver.Accept();

		if (connection == default(NetworkConnection))
		{
			return Mathf.Infinity;
		}
		else
		{
			var connection_id = next_connection_id++;

			numberToConnection.Add(connection_id, connection);
			connectionToNumber.Add(connection, connection_id);
			connection_lists[(int) port].Add(connection);

			return connection_id;
		}
	}

	public void Create (GameObject prefab)
	{
		foreach (var port in drivers.Keys)
		{
			var driver = drivers[port];

			foreach (var connection in connection_lists[port])
			{
				driver.BeginSend(connection, out var writer);
				writer.WriteByte(1);
				Serialization.Serialize(prefab, ref writer);
				driver.EndSend(writer);
			}
		}
	}

	public float ReadInput (InputAction input, float connection_number)
	{
		var connection_id = (uint) connection_number;

		if ( ! inputs.ContainsKey((connection_id,input)))
		{
			return Mathf.Infinity;
		}

		var value = inputs[(connection_id, input)];

		return value;
	}
}
