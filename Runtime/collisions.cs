using UnityEngine;
using System.Collections.Generic;

public class collisions : MonoBehaviour
{
	public List<GameObject> Value = new List<GameObject>();

	void OnCollisionEnter (Collision collision)
	{
		Value.Add(collision.gameObject);
	}
	void OnCollisionExit (Collision collision)
	{
		Value.Remove(collision.gameObject);
	}
	void OnTriggerEnter (Collider collider)
	{
		Value.Add(collider.gameObject);
	}
	void OnTriggerExit (Collider collider)
	{
		Value.Remove(collider.gameObject);
	}
}
