using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System;

public class Web3 : MonoBehaviour
{
	[DllImport("__Internal")] static extern void Init();
	[DllImport("__Internal")] public static extern void Connect();
	[DllImport("__Internal")] public static extern void SwitchNetwork(string chainId);

	public static event Action<string> OnAccountChange;
	public static event Action<string> OnChainChange;

	public static void Initialize ()
	{
		var go = new GameObject("Web3", typeof(Web3));
		Init();
	}

	public void AccountChanged (string account)
	{
		OnAccountChange.Invoke(account);
	}

	public void ChainChanged (string chain)
	{
		OnChainChange(chain);
	}
}
