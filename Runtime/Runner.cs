using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Runner : MonoBehaviour
{
	public static event Action<GameObject> OnEntityCreated;
	public static event Action<GameObject> OnEntityDestroyed;
	public static event Action OnFrameUpdate;

	static GameObject instance;

	public static void Initialize ()
	{
		if (instance == null)
		{
			instance = new GameObject("Runner", typeof(Runner));
			DontDestroyOnLoad(instance);
		}
	}

	public static void Create (GameObject go)
	{
		GameObject.Instantiate(go);
		CreateRecursive(go);
	}

	static void CreateRecursive (GameObject go)
	{
		OnEntityCreated?.Invoke(go);

		foreach (Transform child in go.transform)
		{
			CreateRecursive(child.gameObject);
		}
	}

	public static void Destroy (GameObject go)
	{
		DestroyRecursive(go);
		GameObject.Destroy(go);
	}

	static void DestroyRecursive (GameObject go)
	{
		OnEntityDestroyed?.Invoke(go);

		foreach(Transform child in go.transform)
		{
			DestroyRecursive(child.gameObject);
		}
	}

	void Start ()
	{
		for (var i = 0; i < SceneManager.sceneCount; i++)
		{
			var scene = SceneManager.GetSceneAt(i);
			foreach (var root in scene.GetRootGameObjects())
			{
				CreateRecursive(root);
			}
		}
	}

	void Update ()
	{
		OnFrameUpdate?.Invoke();
	}
}
