using System;
using HarmonyLib;
using UnityEngine;
using System.Linq;
using static System.Reflection.BindingFlags;

namespace M
{
	public class Patches
	{
		public static event Action<GameObject> OnCreate;

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
		static void Create ()
		{
			var harmony = new Harmony("minim.tools");

			var prefix = new HarmonyMethod(typeof(Patches).GetMethod(nameof(Prefix), NonPublic | Static));
			var postfix = new HarmonyMethod(typeof(Patches).GetMethod(nameof(Postfix), NonPublic | Static));

			foreach (var method in typeof(GameObject).GetMethods().Where(x => x.Name == "Instantiate"))
			{
				if (method.IsGenericMethod)
				{
					harmony.Patch(method.MakeGenericMethod(typeof(GameObject)), prefix: prefix);
				}
				else
				{
					harmony.Patch(method, prefix: prefix);
				}
			}

			foreach (var method in typeof(GameObject).GetConstructors())
			{
				harmony.Patch(method, postfix: postfix);
			}
		}
		static bool Prefix (GameObject original)
		{
			OnCreate?.Invoke(original);
			return true;
		}
		static void Postfix (GameObject __instance)
		{
			OnCreate?.Invoke(__instance);
		}
	}
}
