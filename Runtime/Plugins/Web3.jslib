function Init ()
{
	ethereum.on("accountsChanged", ([address]) => unity.SendMessage("Web3", "AccountChanged", address));
	ethereum.on('chainChanged', chain => unity.SendMessage("Web3", "ChainChanged", chain));

	setTimeout(() => unity.SendMessage("Web3", "ChainChanged", ethereum.chainId), 1000);
}

async function Connect ()
{
	let [address] = await ethereum.request({method: "eth_requestAccounts"});

	window.unity.SendMessage("Web3", "AccountChanged", address);
}

async function SwitchNetwork (chain)
{
	await ethereum.request({method: "wallet_switchEthereumChain", params: [{chainId: UTF8ToString(chain)}]});
}

mergeInto(LibraryManager.library, { Init, Connect, SwitchNetwork });
